(function() {

  var APP = angular.module("myAPP",['angular-loading-bar','ngRoute', 'ngAnimate']);

    APP.controller("MainController",function($scope,$location) {
        $scope.location = $location.path();

        $scope.$watch(function() {
            return $location.path();
        }, function(){
            $scope.location = $location.path();
        });
    });

    APP.config(['$routeProvider', function ($routeProvider) {
        $routeProvider
            .when('/', {
                templateUrl: 'partials/home'
            })
            .when('/nos-pizzas', {
                templateUrl: 'partials/pizzas'
            })
            .when('/nous-situer', {
                templateUrl: 'partials/situer'
            })
            .when('/nos-partenaires', {
                templateUrl: 'partials/partenaires'
            })
            .when('/mentions', {
                templateUrl: 'partials/mentions'
            })
            .otherwise({
                redirectTo: '/'
            });
    }]);
})();
